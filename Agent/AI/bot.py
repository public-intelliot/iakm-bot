import os
import sys
import json
import time
import subprocess
import dotenv
dotenv_file = dotenv.find_dotenv("ai.env")
dotenv.load_dotenv(dotenv_file)

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    WHITE = '\033[97m'

print(bcolors.BOLD)
print("Welcome to IAKM")
print("---------------")
print(bcolors.ENDC)

indice =""
agentId = input('Enter your Agent-name: ')
print(bcolors.OKGREEN + "OK" + bcolors.ENDC)

while indice not in ["1","2","3","4"]:
    print("")
    if indice != "": print(bcolors.WARNING + "Wrong answer, please retry !" + bcolors.ENDC)
    print(bcolors.BOLD + bcolors.UNDERLINE  +"1"+ bcolors.ENDC  + bcolors.BOLD + bcolors.OKCYAN  +" Push" + bcolors.ENDC + "      IF you have AI model and you want to push it to IAKM server.")
    print(bcolors.BOLD + bcolors.UNDERLINE  +"2"+ bcolors.ENDC  + bcolors.BOLD + bcolors.OKCYAN  +" Get" + bcolors.ENDC + "       IF you want to get an AI model for a given context.")
    print(bcolors.BOLD + bcolors.UNDERLINE  +"3"+ bcolors.ENDC  + bcolors.BOLD + bcolors.OKCYAN  +" Available" + bcolors.ENDC + " IF you are available to train in a given context.")
    print(bcolors.BOLD + bcolors.UNDERLINE  +"4"+ bcolors.ENDC  + bcolors.BOLD + bcolors.OKCYAN  +" Train" + bcolors.ENDC + "     IF you want to have a new trained AI model for a given context.")
    indice = input('Which IAKM service you want? Enter a number from list above:')
print(bcolors.OKGREEN + "OK")
print(bcolors.ENDC)

uc = "uc-push" if indice =="1" else ("uc-use" if indice =="2" else ("uc-available" if indice =="3" else "uc-train"))
ai_host = "agent-ai-push" if indice =="1" else ("agent-ai-use" if indice =="2" else ("agent-ai-available" if indice =="3" else "agent-ai-train"))

list_files_models = []
if indice =="1":
    basepath = 'models/'
    for entry in os.listdir(basepath):
        path = os.path.join(basepath, entry)
        if os.path.isfile(path):
            list_files_models.append(entry)
    model_index, model_selection = 0,  ""
    print(f"{bcolors.BOLD}{bcolors.UNDERLINE}{model_index}{bcolors.ENDC} --New Model--")
    for file in list_files_models:
        entry = list_files_models[model_index]
        model_index += 1
        print(f"{bcolors.BOLD}{bcolors.UNDERLINE}{model_index}{bcolors.ENDC} {entry}")
    model_selection = input('Which AI-model you want to Push? Enter a number from list above:')
    print(bcolors.OKGREEN + "OK" + bcolors.ENDC)

    selected = int(model_selection)
    if selected == 0: file_model = f"__new__"
    else:             file_model = f"{list_files_models[selected-1]}"
    dotenv.set_key(dotenv_file, "MODEL_FILE", file_model,  quote_mode="never")

context = {
    "entity":"",
    "attributes":{
    }    
}

print(bcolors.BOLD)
print("Context"+ bcolors.ENDC)
print("--------")

print('What is you Map Entity?')
print(bcolors.WHITE + "example: Roundabout, Intersection, Ramp, Segment, Forest..."+ bcolors.ENDC)
entity = input('Enter:')
print(bcolors.OKGREEN + "OK" + bcolors.ENDC)
context["entity"] = entity

print(bcolors.BOLD)
print("Attributes"+bcolors.ENDC)
print("----------" )

print("Enter anytime " + bcolors.BOLD + "c/cancel" + bcolors.ENDC  +" to continue...")
list_keys, list_values = [], []
inAttributes = True

while inAttributes:
    att = input('Enter Key Value:')
    if att in ["B","b","break"]: break
    try:
        list_att = att.split(" ")
        if len(list_att) > 1:
            list_keys.append(list_att[0])
            list_values.append(" ".join(list_att[1:]))
    except:
        print(sys.exc_info())
print(bcolors.OKGREEN + "OK" + bcolors.ENDC)

for i in range(len(list_keys)):
    key = list_keys[i]
    value = list_values[i]
    context["attributes"][key] = value

print(bcolors.ENDC)
print("Your CONTEXT in Json-Format:")
print(bcolors.OKGREEN + json.dumps(context, indent=1))
print(bcolors.ENDC)

file_context = f'context/post/{entity}.json' if uc == "uc-push" else f'context/get/{entity}.json'
with open(file_context, "w") as outfile:
    outfile.write(json.dumps(context))

# os.environ["AI_HOST"] = ai_host
# os.environ["AGENT_ID"] = agentId
# os.environ["MODEL_USAGE"] = uc
dotenv.set_key(dotenv_file, "AI_HOST", ai_host,  quote_mode="never")
dotenv.set_key(dotenv_file, "AGENT_ID", agentId, quote_mode="never")
dotenv.set_key(dotenv_file, "MODEL_USAGE", uc,   quote_mode="never")
dotenv.set_key(dotenv_file, "ENTITY", entity,    quote_mode="never")

print(bcolors.OKCYAN)
print("We are processing your request, please wait...")
print(bcolors.ENDC)
time.sleep(3)
subprocess.call(['sh', './ai-build-run.sh'])