# Infrastructure Assisted Knowledge Management

This project aims to build an AIaaS (Artificial Intelligence-as-a-Service) architecture in order to expose AI services to vehicular agents.

## Intro

AIaaS architecture consists of 2 main entities:

- <strong>IAKM-server:</strong> represents the server which provides AI services
- <strong>IAKM-agent: </strong> represents the client who asks for AI services

## Contents

*   [Deployement requirements](#deployement-requirements)
*   [Getting started](#getting-started)
    *   [IAKM Server](#iakm-server)
    *   [IAKM Agent](#iakm-agent)
    *   [AI Agent](#ai-agent)
*   [Used Technologies](#used-technologies)
*   [Current improvement](#current-improvement)
*   [Contribute](#contribute)
*   [License](#license)

## Deployement requirements

*   [x] [Docker](https://docs.docker.com/engine/) Engine
*   [x] [Docker Compose](https://docs.docker.com/compose/) version (3.7 or above)  

## Getting Started

Clone the master branch of IAKM repository to your local machine:

```
git clone https://gitlab.eurecom.fr/public-intelliot/iakm-bot.git
```

### 1. IAKM Server

IAKM server can be depoyed by build IAKM container on your host and run them inside the dockerized network on your machine as following:

```bash
$ ./server-build-run.sh
```

![Alt Text](https://nextcloud.eurecom.fr/s/HjxZ6oywGZB7PMt/download/server.gif)

<br /><hr />
========================================================================================================================

### 2.IAKM Agent

IAKM agent can be depoyed by build Agent container on your host and run it on your machine as following:

```bash
$ ./agent-build-run.sh
```

<strong>Note:</strong> Configure your agent by changing its environnement file agent.env
<table>
<tr><td colspan=3 align="center"> <strong>agent.env</strong></td></tr>
<tr><td>Environment Variable</td><td>Default</td><td>Description</td></tr>
<tr><td>AGENT_PORT</td><td>8004</td><td><em>Port of  IAKM agent</td></tr>
<tr><td>AGENT_HOST</td><td>agent_iakm</td><td><em>Name of  IAKM agent container</td></tr>
<tr><td>BROKER_PORT</td><td>8883</td><td><em>Port of MQTT broker</td></tr>
<tr><td>BROKER_HOST</td><td>192.168.104.150</td><td><em>IP of MQTT broker</td></tr>
</table>

<br /><hr />
========================================================================================================================

### 3. AI agent

Local AI represents the end-user of our IAKM platform, thus you might want to access directly to IAKM agent interface, this will be a bit complex without knowing what is the required information ...
To solve this complexity! We provides an image ready to access the IAKM platform with the differents usecases; you just have to locate on "AI" directory and run the main script to deploy your IAKM agent:

Agent instance exposes many endpoints service to local-AI, so the latter could use any endpoint according to its needs:

```bash
$ python bot.py
```

![Alt Text](https://nextcloud.eurecom.fr/s/zTzZGWe9SybgwkX/download/iakm-bot-video.gif)

#### 3.1.1 Push-AI:
For instance, an Agent could have a full storage in his local database, so he could ask to push his AI model to be stored in the database of geoserver.

![Alt Text](https://nextcloud.eurecom.fr/s/BnqfPkrT2DWZnon/download/usecase-push.gif)


#### 3.1.2 Get-AI:
An agent could not have all AI models in his local storage, so whenever he do not find a local model, he could ask the geoserver for an AI model according to some context  could have a full storage in his local database, so he could ask to push his AI model to be stored in the database of geoserver.


![Alt Text](https://nextcloud.eurecom.fr/s/YBfpkxZdFy8HGLF/download/usecase-get.gif)


#### 3.1.3 Available-Train-AI:
In order to participate in training process, Agent is able to announce his availability to train in a given context..


![Alt Text](https://nextcloud.eurecom.fr/s/8p9kxAZRW4WDQ8j/download/usecase-available.gif)

#### 3.1.4 Train-AI:
Similar to Get-AI usecase, but in this case the agent wants to have an updated AI-model, so the geoserver once receiving the request will search for  available agents that already announced their availability with the current specific context, and assign the train task to them including the requester (if capable to train), at the end the Global-AI will merge all received trained model in order to have  the best AI averaged model and then send this model to requester and all participants.

![Alt Text](https://nextcloud.eurecom.fr/s/nWFk8WCdtaW3Cq5/download/usecase-train.gif)


<br /><hr />
========================================================================================================================

### 2.4. Agent Endpoints
![Diagram](images/Agent_KM_API.png)


## Used technologies

- <strong>RESTful Node:</strong> for agents internal communication (between local-AIs and agent-KM)
- <strong>MQTT(MOSQUITTO):</strong> for external communication (between Geoserver and agents)
- <strong>MongoDB/GridFS:</strong> For storage of AI models and agents information
- <strong>PySyft:</strong> for federated learning of the trained AI models 
- <strong>Docker/Compose:</strong> helps for scalibility, portability and networking

## Current improvement

- Improving data storage/search by adopting Semantic-AI concept in our AIaaS architecture
- Adding AMQP-broker (RabbitMQ) to ensure the communication among edges    

## Contribute

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
This work is released under Apache 2.0 license. See the [license file](LICENSE.txt) for more details. Contribution and integrations are appreciated.
